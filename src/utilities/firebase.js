import firebase from 'firebase/app';
import 'firebase/auth';

const firebaseConfig = {
	apiKey: 'AIzaSyCQL03cjsZYyj5st39PW08FwgrWwzubhzM',
	authDomain: 'vue-bitumes-course.firebaseapp.com',
	projectId: 'vue-bitumes-course',
	storageBucket: 'vue-bitumes-course.appspot.com',
	messagingSenderId: '900052182561',
	appId: '1:900052182561:web:b1bc1c82d2a09fdbf7a560',
};

firebase.initializeApp(firebaseConfig);

export default firebase;
