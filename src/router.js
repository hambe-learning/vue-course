import { createRouter, createWebHistory } from 'vue-router';
import Calendar from './pages/Calendar';
import Employees from './pages/Employees';
import Home from './pages/Home';
import Markdown from './pages/Markdown';
import Slider from './pages/Slider';

const routes = [
	{ path: '/', component: Home },
	{ path: '/employees', component: Employees },
	{ path: '/calendar', component: Calendar },
	{ path: '/markdown', component: Markdown },
	{ path: '/slider', component: Slider },
];

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = createRouter({
	// 4. Provide the history implementation to use. We are using the hash history for simplicity here.
	history: createWebHistory(),
	routes, // short for `routes: routes`
});

export default router;
